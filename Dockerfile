FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /app

COPY . /app/

RUN pip install -r requirements.txt

VOLUME ["app/database"]
EXPOSE 80
CMD sh init.sh && python3 manage.py run server 0.0.0.0:80
